#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A utility for application logging to Applog. 

See https://truedmp.atlassian.net/wiki/spaces/PA/pages/163874007/ApplogV2

Created on Tue Oct 30 10:02:49 2018

@author: ktp
"""

import os
import sys
import time
import logging
import logging.handlers
import arcommon.settings as stt

from requests_futures.sessions import FuturesSession


MAX_LOG_BYTES = 1000000
MAX_BACKUP = 4
_FORMAT_STR = "%(asctime)s [%(levelname)s] %(message)s"
_SELF_TYPE = "ST03"
_SELF_GROUP = "ASR"
_POST_TIMEOUT = 1 # Applog POST timeout in sec
_MAX_APPLOG_WORKERS = 8


# _applog_session is a single session object providing connection / thread pool 
# behind the scnene to POST messages to the Applog server
_applog_session = FuturesSession(max_workers=_MAX_APPLOG_WORKERS)
_root_logger = None


def init_root_logger(log_level=logging.DEBUG, handlers=None, log_file=None):
    """
    Initialise the application's root logger, on which all other python child
    loggers will send their logs to. This should be called once during application
    startup process to configure your desire log handlers, levels and other
    options. All other AppLogger instances will send their python logs to the
    standard python loggers, which uses the root logger's configurations done
    here.
    
    https://docs.python.org/3/howto/logging-cookbook.html
    
    Params:
        log_level - minimum log level for python's logging facility
        handlers - A list of custom handlers. If given, will append this logger's 
            handlers to include those given. This is useful when, e.g., you 
            deploy on a WSGI server like gunicorn and want the logger to use 
            server's handlers.
            See https://medium.com/@trstringer/logging-flask-and-gunicorn-the-manageable-way-2e6f0b8beb2f
        log_file - If given, write log files using the given log_file as 
            the base name of rotating log file handler. By default each 
            log file is < MAX_LOG_BYTES and will keep at most MAX_BACKUP files.
    """
    global _root_logger
    
    if not _root_logger:
        _root_logger = logging.getLogger()
        
        # add given handlers
        if handlers and isinstance(handlers, list):
            if sum([isinstance(handler, logging.Handler) for handler in handlers]) == len(handlers):
                _root_logger.handlers.extend(handlers)
            else:
                raise TypeError("handlers must be a list containing handler objects")
            
        formatter = logging.Formatter(_FORMAT_STR)
        
        # add console handler
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)
        _root_logger.addHandler(console_handler)
        
        # add rotating file handler
        if log_file and isinstance(log_file, str):
            file_handler = logging.handlers.RotatingFileHandler(
                    log_file, mode='a', maxBytes=MAX_LOG_BYTES, 
                    backupCount=MAX_BACKUP)
            file_handler.setFormatter(formatter)
            _root_logger.addHandler(file_handler)
            
        _root_logger.setLevel(log_level)


class CallDirections:
    """
    Enum for call_direction field
    """
    Internal = 0 # self2self
    Inbound = 1  # alliance2self
    Outbound = 2 # self2alliance
    

class HttpReqMethods:
    """
    Enum for HTTP verbs
    """
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    DEL = "DELETE"
    

def _create_applog_body(
        message, current_time, self_system, self_module, self_function, 
        call_direction, call_req_params, call_req_headers, call_req_methods, 
        call_res_body, call_res_status, call_res_time, call_severity, **kwargs):
    """
    Create the json applog body with mandatory fields and additional fields as
    specified.
    
    Params:
        current_time - int, ms since epoch
        call_direction - CallDirections enum
        call_req_methods - HttpReqMethods enum
        call_res_time - int
        call_severity - logging.[DEBUG, INFO, WARNING, ERROR, CRITICAL] enum
        
        The rest of the fields are all string
    """
    body = {}
    body = {}
    body["message"] = message
    body["start_time"] = current_time               # stores the current time this msg was created
    body["self_type"] = _SELF_TYPE
    body["self_group"] = _SELF_GROUP
    body["self_system"] = self_system
    body["self_module"] = self_module
    body["self_function"] = self_function
    body["call_direction"] = call_direction
    body["call_req_params"] = call_req_params
    body["call_req_headers"] = call_req_headers
    body["call_req_methods"] = call_req_methods
    body["call_res_body"] = call_res_body
    body["call_res_status"] = call_res_status
    body["call_res_time"] = call_res_time           # duration (current_time - begin time)
    body["call_severity"] = int(call_severity / 10) - 1 # applog spec dictates 0-4 range
    body.update(kwargs)
        
    return body


def _get_current_time_ms():
    return int(time.time() * 1000)


def _async_post(url, json_body, logger=None):
    """
    POST to url using a background thread and forget about it, not waiting for
    the result.
    
    Params:
        logger - An instance of Python logger to log in case POST fails.
    """
    # POST and forget, not waiting for result. Any exception that can happen
    # will be raised only if we call the .result() on the returned future object
    # from this post() call (in which we don't). So no exception should be 
    # thrown here. But let's catch it just to be safe. 
    #
    # See https://github.com/ross/requests-futures
    try:
        _applog_session.post(url, json=json_body, timeout=_POST_TIMEOUT)
    except Exception as e:
        logger.warning("Failed to POST to %s: %s" % (url, type(e)))
    

class AppLogger():
    
    
    def __init__(self, name, app_name="", applog_level=logging.DEBUG, log_level=None):
        """
        AppLog logger class. An instance of this class can be created and used
        within each Python module, just like a typicall logger. It will send 
        logging messages to AppLog, as well as to Python's native logger.
        
        Params:
            name - name of the calling python module, e.g., __name__
            app_name - name of the application. If not given, it tries to get
                the name from an environment variable APP_NAME first. If
                that is also not set, it uses the default empty string "" as 
                the name.
            applog_level - minimum log level to send to AppLog. Use the 
                logging.[DEBUG, INFO, WARNING, ERROR, CRITICAL] enum
            log_level - If set, overrides the log_level set during init_root_logger()
                call for this specific logging instance.
        """
        if (not app_name) or not (app_name.strip()):
            app_name = os.getenv("APP_NAME", "")
            
        self._app_name = app_name
        self._name = name
        self._applog_level = applog_level
        self._logger = logging.getLogger(self._name)
        
        if log_level:
            self._logger.setLevel(log_level)
        
        
    def _send_to_applog(
            self, call_level, message, current_time, self_system, self_module, 
            self_function, call_direction, call_req_params, call_req_headers, 
            call_req_methods, call_res_body, call_res_status, begin_time, **kwargs):
        """
        Send the log to AppLog, if applog_level is less than or equal to call_level.
        
        If current_time is None, the current time in ms is computed and used.
        
        If begin_time is either float or int, the "call_res_time" field for AllLog
        is computed as current_time - begin_time. Otherwise this field is set to 0.
        
        Params:
            call_level - logging.[DEBUG, INFO, WARNING, ERROR, CRITICAL] enum
        Returns:
            Current time in ms from epoch.
        """
        
        if self._applog_level <= call_level:
            
            if not current_time:
                current_time = _get_current_time_ms()
        
            if isinstance(begin_time, (int, float)):
                call_res_time = int(current_time - begin_time)
            else:
                call_res_time = 0
                
            body = _create_applog_body(
                    message, current_time, self_system, self_module, self_function, 
                    call_direction, call_req_params, call_req_headers, call_req_methods, 
                    call_res_body, call_res_status, call_res_time, call_level, 
                    **kwargs)
            _async_post(stt.AR_APPLOG_HOST, body, self._logger)
            
        return _get_current_time_ms()
    
    
    def debug(
            self, message, current_time=None, call_direction=CallDirections.Internal, 
            call_req_params="", call_req_headers="", call_req_methods="", 
            call_res_body="", call_res_status="", begin_time=None, **kwargs):
        """
        Debug log to Python logger and Applog. For detailed explanation of each
        AppLog field (and optional fields you want to pass as additional k=v), see
        https://truedmp.atlassian.net/wiki/spaces/PA/pages/163874007/ApplogV2
        
        Params:
            current_time - int, ms since epoch
            call_direction - CallDirections enum
            call_req_params - Request parameters (HTTP call, function call, etc), if any.
            call_req_headers - Request header (HTTP call), if any.
            call_req_methods - HttpReqMethods enum, if any.
            call_res_body - Response body, if any.
            call_res_status - Response status, if any.
            begin_time - int, ms since epoch. If it is either float or int, the 
                "call_res_time" field for AllLog is computed as (current_time - begin_time). 
                Otherwise the "call_res_time" field is set to 0.
        Returns:
             Current time in ms from epoch.
        """
        self._logger.debug(message)
        return self._send_to_applog(
                logging.DEBUG, message, current_time, self._app_name, self._name, 
                sys._getframe(1).f_code.co_name, call_direction, call_req_params, 
                call_req_headers, call_req_methods, call_res_body, call_res_status, 
                begin_time, **kwargs)


    def warning(
            self, message, current_time=None, call_direction=CallDirections.Internal, 
            call_req_params="", call_req_headers="", call_req_methods="", 
            call_res_body="", call_res_status="", begin_time=None, **kwargs):
        """
        Warning log to Python logger and Applog. For detailed explanation of each
        AppLog field (and optional fields you want to pass as additional k=v), see
        https://truedmp.atlassian.net/wiki/spaces/PA/pages/163874007/ApplogV2
        
        Params:
            current_time - int, ms since epoch
            call_direction - CallDirections enum
            call_req_params - Request parameters (HTTP call, function call, etc), if any.
            call_req_headers - Request header (HTTP call), if any.
            call_req_methods - HttpReqMethods enum, if any.
            call_res_body - Response body, if any.
            call_res_status - Response status, if any.
            begin_time - int, ms since epoch. If it is either float or int, the 
                "call_res_time" field for AllLog is computed as (current_time - begin_time). 
                Otherwise the "call_res_time" field is set to 0.
        Returns:
             Current time in ms from epoch.
        """
        self._logger.warning(message)
        return self._send_to_applog(
                logging.WARNING, message, current_time, self._app_name, self._name, 
                sys._getframe(1).f_code.co_name, call_direction, call_req_params, 
                call_req_headers, call_req_methods, call_res_body, call_res_status, 
                begin_time, **kwargs)
        
        
    def info(
            self, message, current_time=None, call_direction=CallDirections.Internal, 
            call_req_params="", call_req_headers="", call_req_methods="", 
            call_res_body="", call_res_status="", begin_time=None, **kwargs):
        """
        Info log to Python logger and Applog. For detailed explanation of each
        AppLog field (and optional fields you want to pass as additional k=v), see
        https://truedmp.atlassian.net/wiki/spaces/PA/pages/163874007/ApplogV2
        
        Params:
            current_time - int, ms since epoch
            call_direction - CallDirections enum
            call_req_params - Request parameters (HTTP call, function call, etc), if any.
            call_req_headers - Request header (HTTP call), if any.
            call_req_methods - HttpReqMethods enum, if any.
            call_res_body - Response body, if any.
            call_res_status - Response status, if any.
            begin_time - int, ms since epoch. If it is either float or int, the 
                "call_res_time" field for AllLog is computed as (current_time - begin_time). 
                Otherwise the "call_res_time" field is set to 0.
        Returns:
             Current time in ms from epoch.
        """
        self._logger.info(message)
        return self._send_to_applog(
                logging.INFO, message, current_time, self._app_name, self._name, 
                sys._getframe(1).f_code.co_name, call_direction, call_req_params, 
                call_req_headers, call_req_methods, call_res_body, call_res_status, 
                begin_time, **kwargs)
        
        
    def error(
            self, message, current_time=None, call_direction=CallDirections.Internal, 
            call_req_params="", call_req_headers="", call_req_methods="", 
            call_res_body="", call_res_status="", begin_time=None, **kwargs):
        """
        Error log to Python logger and Applog. For detailed explanation of each
        AppLog field (and optional fields you want to pass as additional k=v), see
        https://truedmp.atlassian.net/wiki/spaces/PA/pages/163874007/ApplogV2
        
        Params:
            current_time - int, ms since epoch
            call_direction - CallDirections enum
            call_req_params - Request parameters (HTTP call, function call, etc), if any.
            call_req_headers - Request header (HTTP call), if any.
            call_req_methods - HttpReqMethods enum, if any.
            call_res_body - Response body, if any.
            call_res_status - Response status, if any.
            begin_time - int, ms since epoch. If it is either float or int, the 
                "call_res_time" field for AllLog is computed as (current_time - begin_time). 
                Otherwise the "call_res_time" field is set to 0.
        Returns:
             Current time in ms from epoch.
        """
        self._logger.error(message)
        return self._send_to_applog(
                logging.ERROR, message, current_time, self._app_name, self._name, 
                sys._getframe(1).f_code.co_name, call_direction, call_req_params, 
                call_req_headers, call_req_methods, call_res_body, call_res_status, 
                begin_time, **kwargs)
        






