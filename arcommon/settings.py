#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 10:03:15 2018

@author: ktp
"""

import os


AR_APPLOG_HOST = os.getenv("AR_APPLOG_HOST", "http://10.140.0.16:12203/gelf") # default to dev
