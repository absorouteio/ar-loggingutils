#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 10:03:40 2018

@author: ktp
"""

import os
import time
import logging
import requests
import unittest as ut
import unittest.mock as mock
import arcommon.applog_utils as aut
import arcommon.settings as stt


class AppLogUtilsTest(ut.TestCase):
    
    
    @classmethod
    def setUpClass(cls):
        pass


    @classmethod
    def tearDownClass(cls):
        pass
    
    
    @mock.patch("logging.getLogger")
    @mock.patch("logging.handlers.RotatingFileHandler")
    def test_init_root_logger(self, f_handler_mock, logger_mock):
        handler = logging.StreamHandler()
        handlers = [handler]
        log_file = "test_log"
        aut._root_logger = None
        aut.init_root_logger(
                log_level=logging.DEBUG, handlers=handlers, log_file=log_file)
        
        logger_mock.assert_called_once()
        logger_mock.return_value.handlers.extend.assert_called_once_with(handlers)
        logger_mock.return_value.addHandler.assert_any_call(f_handler_mock.return_value)
        f_handler_mock.assert_called_once_with(log_file, mode='a', maxBytes=mock.ANY, backupCount=mock.ANY)
    
    
    def test_create_applog_body_with_default_params(self):
        body = aut._create_applog_body(
                "1", 2, "3", "3.1", "4", 5, "6", "7", "8", "9", "10", 11, 10)
        exp = {
            'message': '1',
            'start_time': 2,
            'self_type': aut._SELF_TYPE,
            'self_group': aut._SELF_GROUP,
            'self_system': '3',
            'self_module': '3.1',
            'self_function': '4',
            'call_direction': 5,
            'call_req_params': '6',
            'call_req_headers': '7',
            'call_req_methods': '8',
            'call_res_body': '9',
            'call_res_status': '10',
            'call_res_time': 11,
            'call_severity': 0
        }
        
        self.assertEqual(exp, body)
        
        
    def test_create_applog_body_with_optional_params(self):
        body = aut._create_applog_body(
                "1", 2, "3", "3.1", "4", 5, "6", "7", "8", "9", "10", 11, 10, k1="v1", 
                k2=123)
        exp = {
            'message': '1',
            'start_time': 2,
            'self_type': aut._SELF_TYPE,
            'self_group': aut._SELF_GROUP,
            'self_system': '3',
            'self_module': '3.1',
            'self_function': '4',
            'call_direction': 5,
            'call_req_params': '6',
            'call_req_headers': '7',
            'call_req_methods': '8',
            'call_res_body': '9',
            'call_res_status': '10',
            'call_res_time': 11,
            'call_severity': 0,
            'k1': 'v1',
            'k2': 123
        }
        
        self.assertEqual(exp, body)
        
        
    @mock.patch.dict(os.environ, {'APP_NAME': 'test_app_name'})
    def test_setting_default_logger_appname(self):
        logger = aut.AppLogger(__name__)
        self.assertEqual(logger._app_name, "test_app_name")
            
            
    @mock.patch.dict(os.environ, {'APP_NAME': 'test_app_name'})
    def test_setting_logger_appname(self):
        logger = aut.AppLogger(__name__, app_name="overriding_name")
        self.assertEqual(logger._app_name, "overriding_name")
        
        
    @mock.patch("logging.Logger.setLevel")
    def test_overriding_logger_level(self, setLevel_mock):
        aut.AppLogger("test", log_level=logging.ERROR)
        setLevel_mock.assert_called_once_with(logging.ERROR)
            
            
#    @ut.skip("")
    def test_async_post(self):
        # Just test posting to some public site that allows posting random shit
        # to, and assert that no warning was logged. Alternatively you can just
        # mock requests.post if you don't want to really POST to any actual site.
#        test_url = "https://ptsv2.com/t/tjher-1554801646/post"
        test_url = "https://postman-echo.com/post"
        body = "blaaa"
        logger_mock = mock.MagicMock()
        latencies = []
        rounds = 100
        
        for i in range(rounds):
            start = time.time()
            aut._async_post(test_url, body, logger=logger_mock)
            latencies.append(time.time() - start)
        
        avg_latency = sum(latencies) / rounds
        self.assertTrue(avg_latency < 0.002)  # ensure call didn't block for too long
        logger_mock.warning.assert_not_called() # ensure POST succeeded
        
        
    @mock.patch("requests_futures.sessions.FuturesSession.post")
    def test_async_post_error(self, post_mock):
        e = requests.exceptions.InvalidURL()
        post_mock.side_effect = e
        test_url = "www.bla.com"
        body = "blaaa"
        logger_mock = mock.MagicMock()
        aut._async_post(test_url, body, logger=logger_mock)
        
        post_mock.assert_called_once_with(test_url, json=body, timeout=aut._POST_TIMEOUT)
        logger_mock.warning.assert_called_once_with("Failed to POST to %s: %s" % (test_url, type(e)))
        
    
    @mock.patch("requests_futures.sessions.FuturesSession.post")
    def test_debug_log(self, post_mock):
        rsp = requests.Response()
        rsp.status_code = 202
        post_mock.return_value = rsp
        app_name = "test_app"
        name = "test_module"
        msg = "test_debug_msg"
        current_time = int(time.time() * 1000)
        logger = aut.AppLogger(name, app_name, applog_level=logging.DEBUG)
        rsp = logger.debug(msg, current_time=current_time)
        
        exp_applog_body = {
                'message': msg, 'start_time': current_time, 'self_type': 'ST03', 
                'self_group': 'ASR', 'self_system': app_name, 'self_module': name, 
                'self_function': 'test_debug_log', 'call_direction': 0, 
                'call_req_params': '', 'call_req_headers': '', 'call_req_methods': '', 
                'call_res_body': '', 'call_res_status': '', 'call_res_time': 0, 
                'call_severity': 0}
        
        self.assertTrue(rsp >= current_time)
        post_mock.assert_called_once_with(
                stt.AR_APPLOG_HOST, json=exp_applog_body, timeout=aut._POST_TIMEOUT)
        
        
    @mock.patch("requests_futures.sessions.FuturesSession.post")
    def test_debug_log_with_begin_time(self, post_mock):
        rsp = requests.Response()
        rsp.status_code = 202
        post_mock.return_value = rsp
        
        app_name = "test_app"
        name = "test_module"
        logger = aut.AppLogger(name, app_name, applog_level=logging.DEBUG)
        start_ts = logger.debug("begin")
        time.sleep(1)
        post_mock.reset_mock()
        
        stop_ts = logger.debug("stop", begin_time=start_ts)
        
        self.assertTrue(stop_ts >= start_ts + 1000)
        
        args = post_mock.call_args
        call_res_time = args[1]["json"]["call_res_time"]
        self.assertTrue((call_res_time >= 1000) and (call_res_time < 2000))
        
        
    @mock.patch("requests_futures.sessions.FuturesSession.post")
    def test_debug_log_with_applog_at_error(self, post_mock):
        rsp = requests.Response()
        rsp.status_code = 202
        post_mock.return_value = rsp
        
        app_name = "test_app"
        name = "test_module"
        msg = "test_debug_msg"
        current_time = int(time.time() * 1000)
        logger = aut.AppLogger(name, app_name, applog_level=logging.ERROR)
        rsp = logger.debug(msg, current_time=current_time)
        
        self.assertTrue(rsp >= current_time)
        post_mock.assert_not_called()
        
        
    @mock.patch("requests_futures.sessions.FuturesSession.post")
    def test_info_log(self, post_mock):
        rsp = requests.Response()
        rsp.status_code = 202
        post_mock.return_value = rsp
        
        app_name = "test_app"
        name = "test_module"
        msg = "test_info_msg"
        current_time = int(time.time() * 1000)
        logger = aut.AppLogger(name, app_name, applog_level=logging.DEBUG)
        rsp = logger.info(msg, current_time=current_time)
        
        exp_applog_body = {
                'message': msg, 'start_time': current_time, 'self_type': 'ST03', 
                'self_group': 'ASR', 'self_system': app_name, 'self_module': name, 
                'self_function': 'test_info_log', 'call_direction': 0, 
                'call_req_params': '', 'call_req_headers': '', 'call_req_methods': '', 
                'call_res_body': '', 'call_res_status': '', 'call_res_time': 0, 
                'call_severity': 1}
        
        self.assertTrue(rsp >= current_time)
        post_mock.assert_called_once_with(
                stt.AR_APPLOG_HOST, json=exp_applog_body, timeout=aut._POST_TIMEOUT)
        
        
    @mock.patch("requests_futures.sessions.FuturesSession.post")
    def test_warning_log(self, post_mock):
        rsp = requests.Response()
        rsp.status_code = 202
        post_mock.return_value = rsp
        
        app_name = "test_app"
        name = "test_module"
        msg = "test_warning_msg"
        current_time = int(time.time() * 1000)
        logger = aut.AppLogger(name, app_name, applog_level=logging.DEBUG)
        rsp = logger.warning(msg, current_time=current_time)
        
        exp_applog_body = {
                'message': msg, 'start_time': current_time, 'self_type': 'ST03', 
                'self_group': 'ASR', 'self_system': app_name, 'self_module': name, 
                'self_function': 'test_warning_log', 'call_direction': 0, 
                'call_req_params': '', 'call_req_headers': '', 'call_req_methods': '', 
                'call_res_body': '', 'call_res_status': '', 'call_res_time': 0, 
                'call_severity': 2}
        
        self.assertTrue(rsp >= current_time)
        post_mock.assert_called_once_with(
                stt.AR_APPLOG_HOST, json=exp_applog_body, timeout=aut._POST_TIMEOUT)
        
        
    @mock.patch("requests_futures.sessions.FuturesSession.post")
    def test_error_log(self, post_mock):
        rsp = requests.Response()
        rsp.status_code = 202
        post_mock.return_value = rsp
        
        app_name = "test_app"
        name = "test_module"
        msg = "test_error_msg"
        current_time = int(time.time() * 1000)
        logger = aut.AppLogger(name, app_name, applog_level=logging.DEBUG)
        rsp = logger.error(msg, current_time=current_time)
        
        exp_applog_body = {
                'message': msg, 'start_time': current_time, 'self_type': 'ST03', 
                'self_group': 'ASR', 'self_system': app_name, 'self_module': name, 
                'self_function': 'test_error_log', 'call_direction': 0, 
                'call_req_params': '', 'call_req_headers': '', 'call_req_methods': '', 
                'call_res_body': '', 'call_res_status': '', 'call_res_time': 0, 
                'call_severity': 3}
        
        self.assertTrue(rsp >= current_time)
        post_mock.assert_called_once_with(
                stt.AR_APPLOG_HOST, json=exp_applog_body, timeout=aut._POST_TIMEOUT)
    

if __name__ == '__main__':
    ut.main()

    
    
# Some PoC code. Don't care about it.
    
#import time
#from requests_futures.sessions import FuturesSession
#
#
#session = FuturesSession()
#    
#url = "https://ptsv2.com/t/o7hh4-1554864532/post"
#json_body = {"I'm destined": "to something great"}
#future_one = session.post(url, json=json_body, timeout=1)
#
#start = time.time()
#for i in range(100):
#    json_body = {
#            "I'm destined": "to something great",
#            "loop": i
#    }
#    session.post(url, json=json_body, timeout=1)
#
#end = time.time()
#avg_latency = (end - start) / 1000
#print(avg_latency)


