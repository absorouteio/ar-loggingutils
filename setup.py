from setuptools import setup, find_packages

REQUIRED_PACKAGES = [
    'requests==2.22.0',
    'requests-futures==1.0.0'
]

setup(name='rcomloggingutils',
      version='1.1.3',
      packages=find_packages(exclude=('tests',)),
      url='https://bitbucket.org/truedmp/rcomloggingutils.git',
      include_package_data=True,
      description='Rcom common utils for logging to Applog',
      author='Ktawut T.Pijarn',
      author_email='ktawut_tap@truecorp.co.th',
      install_requires=REQUIRED_PACKAGES,
      zip_safe=False)

